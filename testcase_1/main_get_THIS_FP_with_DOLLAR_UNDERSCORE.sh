#!/usr/bin/env bash

if [[ "${_}" == "${0}" ]] ; then
    # this file is executed
    RAW_FP="${0}"
    THIS_FP="$(readlink -f "${RAW_FP}")"
    CONTEXT='execution'
else
    # this file is `source`d
    RAW_FP="${_}"  # aka `$_`
    THIS_FP="$(readlink -f "${RAW_FP}")"
    CONTEXT='source'
fi

THIS_DIR="$(dirname "${THIS_FP}")"
THIS_FN="$(basename "${THIS_FP}")"
MESSAGE_PREFIX="${THIS_FN}:"

### ------------------------------------------------------------------
### protect from `source`ing
### ------------------------------------------------------------------

declare -r RAW_FP_0="${RAW_FP}"
declare -r CONTEXT_0="${CONTEXT}"
declare -r THIS_FP_0="${THIS_FP}"
declare -r THIS_DIR_0="${THIS_DIR}"
declare -r THIS_FN_0="${THIS_FN}"
declare -r MESSAGE_PREFIX_0="${MESSAGE_PREFIX}"

### ------------------------------------------------------------------
### do `source`ing
### ------------------------------------------------------------------

SOURCE_FN='function_get_THIS_FP_with_BASH_SOURCE.sh'
SOURCE_FP="${THIS_DIR}/${SOURCE_FN}"

# if [[ "${_}" == "${RAW_FP}" ]] ; then
if [[ "${CONTEXT_0}" == 'execution' ]] ; then
    echo "${MESSAGE_PREFIX_0} about to source ${SOURCE_FP}"
fi

source "${SOURCE_FP}"
RAW_FP="${RAW_FP_0}"

if [[ "${CONTEXT_0}" == 'execution' ]] ; then
    echo "${MESSAGE_PREFIX_0} finished source ${SOURCE_FP}"
fi

SOURCE_FN='function_get_THIS_FP_with_DOLLAR_UNDERSCORE.sh'
SOURCE_FP="${THIS_DIR}/${SOURCE_FN}"

if [[ "${CONTEXT_0}" == 'execution' ]] ; then
    echo "${MESSAGE_PREFIX_0} about to source ${SOURCE_FP}"
fi

source "${SOURCE_FP}"
RAW_FP="${RAW_FP_0}"

if [[ "${CONTEXT_0}" == 'execution' ]] ; then
    echo "${MESSAGE_PREFIX_0} finished source ${SOURCE_FP}"
fi

### ------------------------------------------------------------------
### restore after `source`ing
### ------------------------------------------------------------------

RAW_FP="${RAW_FP_0}"
CONTEXT="${CONTEXT_0}"
THIS_FP="${THIS_FP_0}"
THIS_DIR="${THIS_DIR_0}"
THIS_FN="${THIS_FN_0}"
MESSAGE_PREFIX="${MESSAGE_PREFIX_0}"

### ------------------------------------------------------------------
### `source` fence
### ------------------------------------------------------------------

### I.e., don't run any following lines (to EOF) if this file is `source`d

# if [[ "${_}" != "${RAW_FP}" ]] ; then
# fails in execution with
# > line 73: return: can only `return' from a function or sourced script
if [[ "${CONTEXT}" == 'source' ]] ; then
    return 0
fi

### ------------------------------------------------------------------
### payload
### ------------------------------------------------------------------

echo "${MESSAGE_PREFIX} 1st:"
echo -e "\tTHIS_FP='${THIS_FP}'"
echo -e "\tCONTEXT='${CONTEXT}'"
echo # newline

echo "${MESSAGE_PREFIX} about to call bar"
bar
echo "${MESSAGE_PREFIX} finished call bar"
echo # newline

echo "${MESSAGE_PREFIX} about to call foo"
foo
echo "${MESSAGE_PREFIX} finished call foo"
echo # newline

echo "${MESSAGE_PREFIX} call bar again"
bar
echo "${MESSAGE_PREFIX} called bar"
echo # newline

echo "${MESSAGE_PREFIX} call foo again"
foo
echo "${MESSAGE_PREFIX} called foo"
echo # newline

echo "${MESSAGE_PREFIX} exiting: be sure to create new shell!"
exit 0
