#!/usr/bin/env bash

### ASSERT: this file is only to be `source`d
RAW_FP="${BASH_SOURCE}"               # define read-write like caller
THIS_FP="$(readlink -f "${RAW_FP}")"
CONTEXT='source'
THIS_FN="$(basename "${THIS_FP}")"

# MESSAGE_PREFIX="${THIS_FN}:"
# echo "${MESSAGE_PREFIX} 1st:"
# above would be convenient, but overwrites global/caller. Instead, the more verbose

# echo "$(basename ${BASH_SOURCE}): 1st:"
# echo -e "\tTHIS_FP='${THIS_FP}'"
# echo -e "\tCONTEXT='${CONTEXT}'"

function foo() {
    # MESSAGE_PREFIX is safe here, because it's local ... but that only works inside functions.
    local -r MESSAGE_PREFIX="$(basename ${BASH_SOURCE})::${FUNCNAME[0]}:"
    echo "${MESSAGE_PREFIX} you called 'foo' @ $(date)"
}
