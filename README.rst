++++++++++++++++++++
test getting THIS_FP
++++++++++++++++++++

.. contents:: table of contents

summary
+++++++

Investigating how to support my possibly-idiotic messaging idiom (aka *PIMI*) for ``bash`` scripts that are both ``source``\ able and (normally) executable.

details
+++++++

.. _special parameter: https://www.gnu.org/software/bash/manual/html_node/Special-Parameters.html

possibly-idiotic messaging idiom
================================

I have these possibly idiotic preferences:

1. I like to see certain information output by my bash scripts: notably, which script (and which function in a script) is active (or, at least, generating output) at any given time.
#. ... but I very rarely want to see *all* the information generated by the ``set -o`` options I have tried (e.g., ``set -x`` or ``set -v``).

Rather than learning how to bend ``set -o`` to my will, I have instead developed the following PIMI:

1. I begin most of my scripts' constants section with something like:

    ::

        ### messaging

        THIS_FP="${0}"
        THIS_FN="$(basename ${THIS_FP})"
        THIS_DIR="(readlink -f $(dirname ${THIS_FP})"
        MESSAGE_PREFIX="${THIS_FN}:"
        ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

2. I begin most of my scripts' functions with something like:

    ::

        local -r MESSAGE_PREFIX="${MESSAGE_PREFIX}:${FUNCNAME[0]}:"
        local -r ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

3. I output messages like

    ::

        echo "${MESSAGE_PREFIX} something worth noting"
        >&2 echo "${ERROR_PREFIX} something very bad"

The key moves here are to get

* the path to the currently-executing script using the `special parameter`_ ``0`` (better known as ``$0``)
* the name of the currently-executing function using ``${FUNCNAME[0]}``

This generally works well, mostly because I'm not so insane as to write huge apps in ``bash``. Problems that I've dealt with in past mostly involve locals clobbering globals, which I can generally workaround with (e.g.) ``declare -g`` or (more often) ``declare -r``, though the latter `prevents local overrides <https://lists.gnu.org/archive/html/bug-bash/2011-02/msg00154.html>`_. (Note also that the bash builtins ``declare`` and ``typeset`` are synonyms, and the latter is often more searchable.)

usecase
=======

However I recently started writing more code

1. with executable testcases (not *this* code, however)
2. with testcases modularized as functions
3. which I want to be able to both run (normally) *and* ``source``

This caused problems for the above crude PIMI (PIMI 1.0?), since when ``source``\ ing, ``$0 == /bin/bash`` or equivalent. I also continue to want to be able to

4. run code in one-liners (e.g., ``foo ; bar ; baz``)
5. run code via relative paths as well as fully-qualified paths (aka *FQPs*)

While investigating attempts to satisfy the 1st 3 requirement, I encountered problems with the latter 2. Hence I wrote the following repo/testcases to systematically examine how to better satisfy all the above usecase requirements.

``$0`` alternatives
===================

The problem is, roughly,

1. how to detect when a script is being either executed (i.e., the normal mode) or being ``source``\d. One can continue to use ``${0}`` to get the path to a script being executed.
#. how to get the path to a script being ``source``\d.
#. ... while satisfying all the above requirements

From some websearch, I noted 2 main alternatives (for ``bash``--note I am not much interested in portability to other shell scripting dialects):

* the `builtin variable <https://www.gnu.org/software/bash/manual/html_node/Bash-Variables.html>`_ ``BASH_SOURCE``. This is an array variable in all but very old ``bash`` versions, so ``BASH_SOURCE == BASH_SOURCE[0]``
* the `special parameter`_ ``_`` (better known as ``$_``)

testcases
+++++++++

overview
========

Each testcase represented by a dir/folder containing:

* copies of (each of) ``bash`` files that worked for that case
* 1 output file containing transcript of run from console

Where exercised, each *sub-testcase* (e.g., {``BASH_SOURCE``, FQP}) of a given testcase must be run in a different shell from every other sub-testcase.

testcase 1: execution by FQP and 1-level relpath
================================================

.. Note double underscores following next hyperlink: that makes it an `anonymous hyperlink reference`, and prevents docutils/restview from throwing a `Duplicate explicit target name` error! Thank you https://stackoverflow.com/a/14067756/915044

Tested with both ``BASH_SOURCE`` and ``$_``, latter deprecated: see `output <./testcase_1/output_1.txt>`__.

1.1. ``BASH_SOURCE``, FQP
-------------------------

::

    $ /path/to/repo/main_get_THIS_FP_with_BASH_SOURCE.sh
    + expected output from main_get_THIS_FP_with_BASH_SOURCE.sh::main
    + expected output from function_get_THIS_FP_with_BASH_SOURCE.sh
    - failed function_get_THIS_FP_with_DOLLAR_UNDERSCORE.sh::bar

1.2. ``BASH_SOURCE``, relpath
-----------------------------

::

    $ pushd /path/to/repo/
    $ ./main_get_THIS_FP_with_BASH_SOURCE.sh
    + expected output from main_get_THIS_FP_with_BASH_SOURCE.sh::main
    + expected output from function_get_THIS_FP_with_BASH_SOURCE.sh
    - failed function_get_THIS_FP_with_DOLLAR_UNDERSCORE.sh::bar

1.3. ``DOLLAR_UNDERSCORE``, FQP
-------------------------------

::

    $ /path/to/repo/main_get_THIS_FP_with_DOLLAR_UNDERSCORE.sh
    + expected output from main_get_THIS_FP_with_BASH_SOURCE.sh::main
    + expected output from function_get_THIS_FP_with_BASH_SOURCE.sh
    - failed function_get_THIS_FP_with_DOLLAR_UNDERSCORE.sh::bar

1.4. ``DOLLAR_UNDERSCORE``, relpath
-----------------------------------

::

    $ pushd /path/to/repo/
    $ ./main_get_THIS_FP_with_DOLLAR_UNDERSCORE.sh
    + expected output from main_get_THIS_FP_with_BASH_SOURCE.sh::main
    + expected output from function_get_THIS_FP_with_BASH_SOURCE.sh
    - failed function_get_THIS_FP_with_DOLLAR_UNDERSCORE.sh::bar

testcase 2: ``source``\ing by FQP and 1-level relpath
=====================================================

Tested with both ``BASH_SOURCE`` and ``$_``, latter deprecated: see `output <./testcase_2/output_2.txt>`__.

2.1. ``BASH_SOURCE``, FQP
-------------------------

::

    $ source /path/to/repo/main_get_THIS_FP_with_BASH_SOURCE.sh # FQ path
    + expected/null output
    $ foo
    + expected output from function_get_THIS_FP_with_BASH_SOURCE.sh::foo
    $ bar
    - failed function_get_THIS_FP_with_DOLLAR_UNDERSCORE.sh::bar

2.2. ``BASH_SOURCE``, relpath
-----------------------------

::

    $ pushd /path/to/repo/
    $ source ./main_get_THIS_FP_with_BASH_SOURCE.sh
    + expected/null output
    $ foo
    + expected output from function_get_THIS_FP_with_BASH_SOURCE.sh::foo
    $ bar
    - failed function_get_THIS_FP_with_DOLLAR_UNDERSCORE.sh::bar

2.3. ``DOLLAR_UNDERSCORE``, FQP
-------------------------------

::

    $ source /path/to/repo/main_get_THIS_FP_with_DOLLAR_UNDERSCORE.sh # FQ path
    - error output
    $ foo
    - error output
    $ bar
    - error output

2.4. ``DOLLAR_UNDERSCORE``, relpath
-----------------------------------

::

    $ pushd /path/to/repo/
    $ source ./main_get_THIS_FP_with_DOLLAR_UNDERSCORE.sh
    - error output
    $ foo
    - error output
    $ bar
    - error output

testcase 3: sequenced execution by FQP and 1-level relpath
==========================================================

Tested with both ``BASH_SOURCE`` and ``$_``, latter deprecated: see `output <./testcase_3/output_3.txt>`__.

3.1. ``BASH_SOURCE``, FQP
-------------------------

::

    $ date ; /path/to/repo/main_get_THIS_FP_with_BASH_SOURCE.sh # FQ path
    + expected output from main_get_THIS_FP_with_BASH_SOURCE.sh::main
    + expected output from function_get_THIS_FP_with_BASH_SOURCE.sh
    - failed function_get_THIS_FP_with_DOLLAR_UNDERSCORE.sh::bar

3.2. ``BASH_SOURCE``, relpath
-----------------------------

::

    $ pushd /path/to/repo/
    $ date ; ./main_get_THIS_FP_with_BASH_SOURCE.sh # relpath
    + expected output from main_get_THIS_FP_with_BASH_SOURCE.sh::main
    + expected output from function_get_THIS_FP_with_BASH_SOURCE.sh
    - failed function_get_THIS_FP_with_DOLLAR_UNDERSCORE.sh::bar

3.3. ``DOLLAR_UNDERSCORE``, FQP
-------------------------------

::

    $ date ; /path/to/repo/main_get_THIS_FP_with_DOLLAR_UNDERSCORE.sh # FQ path
    + expected output from main_get_THIS_FP_with_DOLLAR_UNDERSCORE.sh::main
    + expected output from function_get_THIS_FP_with_BASH_SOURCE.sh
    - failed function_get_THIS_FP_with_DOLLAR_UNDERSCORE.sh::bar

3.4. ``DOLLAR_UNDERSCORE``, relpath
-----------------------------------

::

    $ pushd /path/to/repo/
    $ date ; ./main_get_THIS_FP_with_DOLLAR_UNDERSCORE.sh # relpath
    + expected output from main_get_THIS_FP_with_DOLLAR_UNDERSCORE.sh::main
    + expected output from function_get_THIS_FP_with_BASH_SOURCE.sh
    - failed function_get_THIS_FP_with_DOLLAR_UNDERSCORE.sh::bar

testcase 4: sequenced ``source``\ing by FQP and 1-level relpath
===============================================================

Tested with both ``BASH_SOURCE`` and ``$_``, latter deprecated: see `output <./testcase_4/output_4.txt>`__.

4.1. ``BASH_SOURCE``, FQP
-------------------------

::

    $ date ; source /path/to/repo/main_get_THIS_FP_with_BASH_SOURCE.sh
    + expected/null output
    $ date ; foo
    + expected output from function_get_THIS_FP_with_BASH_SOURCE.sh
    $ date ; bar
    - failed function_get_THIS_FP_with_DOLLAR_UNDERSCORE.sh::bar

4.2. ``BASH_SOURCE``, relpath
-----------------------------

::

    $ pushd /path/to/repo/
    $ date ; source ./main_get_THIS_FP_with_BASH_SOURCE.sh
    + expected/null output
    $ date ; foo
    + expected output from function_get_THIS_FP_with_BASH_SOURCE.sh
    $ date ; bar
    - failed function_get_THIS_FP_with_DOLLAR_UNDERSCORE.sh::bar

4.3. ``DOLLAR_UNDERSCORE``, FQP
-------------------------------

::

    $ date ; source /path/to/repo/main_get_THIS_FP_with_DOLLAR_UNDERSCORE.sh
    - error output
    $ date ; foo
    - error output
    $ date ; bar
    - error output

4.4. ``DOLLAR_UNDERSCORE``, relpath
-----------------------------------

::

    $ pushd /path/to/repo/
    $ date ; source ./main_get_THIS_FP_with_DOLLAR_UNDERSCORE.sh
    + expected/null output
    $ date ; foo
    + expected output from function_get_THIS_FP_with_BASH_SOURCE.sh
    $ date ; bar
    - failed function_get_THIS_FP_with_DOLLAR_UNDERSCORE.sh::bar

TODO
++++

1. Automate testcases!
#. Retire testcases using special parameter ``$_``: it just sucks.
#. Test additional problematic cases, including the following (adapted from claimed-to-handle cases in an `SO community wiki`_):

    1. call via multiple-depth relpath, e.g. ``./some/folder/script``. Currently (testcases 1-4) I only test single-depth relpaths.
    #. call via symlinks (single- and multiple-depth)
    #. call ``when arg $0 is modified from caller``: not sure what that's about
    #. call FQPs with intervening ``.`` and ``..``, e.g. ``/some/path/../../another/./path/script``
    #. call relpaths with intervening ``.`` and ``..``

#. Test improved getting of ``THIS_DIR``. Options include (not exclusively):

    1. Following very slightly adapted from that `SO community wiki`_. Note this is only one of several answers to a question, and not even the most-voted answer, but it looks best to me.

            ::

                script_path=''                                  # string not constant
                pushd . > /dev/null                             # why? what does this do?
                script_path="${BASH_SOURCE[0]}"
                # note commenter asserts single `[]` more efficient here and following line than `[[]]`
                if [[ -h "${script_path}" ]] ; then             # -h == -L == isa(symlink)
                    while [[ -h "${script_path}" ]] ; do
                        cd "$(dirname "${script_path}")"        # why not `> /dev/null` here too?
                        script_path="$(readlink "${script_path}")"
                    done
                fi
                cd "$(dirname "${script_path}")" > /dev/null
                script_path="$(pwd)"                            # so what was the point of previous gymnastics?
                # TODO: handle error claimed @ https://stackoverflow.com/questions/59895/getting-the-source-directory-of-a-bash-script-from-within/179231#comment18043510_179231
                popd  > /dev/null

    2. Following from Dan Moulding's `comment to previous answer <https://stackoverflow.com/questions/59895/getting-the-source-directory-of-a-bash-script-from-within/179231#comment9536302_179231>`_ but that was a one-liner (``DIR="$(cd -P "$(dirname "${BASH_SOURCE[0]}")" && pwd)"``) which I'm not sure will translate inside a function definition:

            ::

                # rewritten as function taking constant CANDIDATE_PATH as argument
                # CANDIDATE_PATH will usually == BASH_SOURCE[0]
                function get_script_dir() {
                    CANDIDATE_PATH="${0}"       # ASSERT: constant
                    # TODO: test -z , -d , etc
                    local retval=''
                    local dir_name="$(dirname "${CANDIDATE_PATH}")"
                    # from `info bash` section='SHELL BUILTIN COMMANDS':
                    # > The -P option causes cd to use the physical directory structure
                    # by resolving symbolic links while traversing dir and
                    # before processing instances of '..' in dir
                    builtin cd -P "${dir_name}" # `builtin` is also ... a `bash` builtin :-)
                    # execute following only if previous `cd` exit status == 0
                    if (( ? == 0 )) ; then
                        retval="$(pwd)"
                    else
                        retval="ERROR: cannot get true dir from '${CANDIDATE_PATH}'"
                    fi
                    echo "${retval}"  # kludgy `bash` return
                }

    3. Following from `ostricher's blog <http://www.ostricher.com/2014/10/the-right-way-to-get-the-directory-of-a-bash-script/>`_ is a variation on the previous option. Note its caveat: ``It is not robust if you cd to a different directory before calling the function``.

            ::

                # rewritten as function taking constant CANDIDATE_PATH as argument
                # CANDIDATE_PATH will usually == BASH_SOURCE[0]
                function get_script_dir() {
                    CANDIDATE_PATH="${0}"       # ASSERT: constant
                    # TODO: test -z , -d , etc
                    local script_dir=''
                    local script_path="${BASH_SOURCE[0]}"
                    while [ -h "${script_path}" ] ; do   # -h == -L == isa(symlink)
                        # from `info bash` section='SHELL BUILTIN COMMANDS':
                        # > The -P option causes cd to use the physical directory structure
                        # by resolving symbolic links while traversing dir and
                        # before processing instances of '..' in dir
                        script_dir="$( cd -P "$( dirname "${script_path}" )" && pwd )"
                        script_path="$( readlink "${script_path}" )"
                        [[ "${script_path}" != '/*' ]] && script_path="${script_dir}/${script_path}"
                    done
                    # if following `cd -P` fails, we die without hitting `pwd`?
                    builtin cd -P "$( dirname "${script_path}" )" # `builtin` is also ... a `bash` builtin :-)
                    pwd
                }

       *TODO:* make this more readable!

.. _SO community wiki: https://stackoverflow.com/a/179231/915044
